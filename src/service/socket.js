import { io } from 'socket.io-client'

export let socket = null

export function connect(id){
    if(socket == null)
        return socket = io(`localhost:4000/dooze?id=${id}`, {reconnect: true})
    else
        return socket
}