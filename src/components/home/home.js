import { Col, Container, Row } from 'react-bootstrap'
import classes from './home.module.css'
import { useForm } from 'react-hook-form'
import axios from 'axios'
import { useHistory } from 'react-router'

const Home = () => {

    const history = useHistory()

    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm();
    const onSubmit = (data) => {
        axios.post('http://localhost:3000/auth/signup',{...data})
        .then(response => {
            let id = response.data.data._id
            console.log(id);
            history.push('games',{id})
        })
        .catch(err => {
            console.log(err);
        })
    };


    return (
        <Container className={classes.main}>
            <Row className='h-25 d-flex justify-content-center align-items-center'>
                <Col>
                    <h1>DOOZE</h1>
                </Col>
            </Row>
            <Row className='h-75 '>
                <Col xl={6} className={`${classes.test} h-100 d-flex justify-content-center align-items-center`}>
                    <Container className='h-100' >
                        <Row style={{height: '10%'}} ></Row>
                        <Row className='h-75'>
                            <Col xl={2}></Col>
                            <Col xl={8} className={classes.father} >
                                <form onSubmit={handleSubmit(onSubmit)} className={classes.form}  >
                                    <h1>Sign up</h1>
                                    <input {...register('email')} type='email' placeholder='Email' />
                                    <input {...register('password')} type='password' placeholder='password' />
                                    <input type='submit' />
                                </form>
                            </Col>
                            <Col xl={2}></Col>
                        </Row>
                    </Container>
                </Col>
                <Col xl={6} className='' >
                <Container className='h-100' >
                        <Row style={{height: '10%'}} ></Row>
                        <Row className='h-75'>
                            <Col xl={2}></Col>
                            <Col xl={8} className={classes.father} >
                                <form className={classes.form}>
                                    <h1>Log in</h1>
                                    <input type='email' placeholder='Email' />
                                    <input type='password' placeholder='password' />
                                    <input type='submit' />
                                </form>
                            </Col>
                            <Col xl={2}></Col>
                        </Row>
                    </Container>
                </Col>
            </Row>
        </Container>
    )
}

export default Home
