import { useHistory, useLocation } from 'react-router-dom'
import classes from './games.module.css';
import { Container } from 'react-bootstrap'
import { useEffect } from 'react';
import { connect } from '../../service/socket';
import { useState } from 'react';
import { useRef } from 'react';

const Games = () => {

    const location = useLocation()
    const history = useHistory()
    const createGameInputRef = useRef(null)

    const [state, setState] = useState([])

    console.log(location);

    useEffect(() => {
        
        let     socket = connect(location.state.id)

        socket.on('open-game', (data) => {
            console.log(data.data);
            setState(data.data)
        })

        socket.on('create-game', (data) => {
            console.log(" --- > ", data.data);
            setState(old => [...old, {_id: data.data.id, name: data.data.name}])
        })

        socket.on('create-game-owner', data => {
            console.log(">>>>>", data.data);
          history.push(`/games/${data.data.id}`,{id: location.state.id, name: data.data.name})

        })

        return () => {}
    }, [])

    const goDoozePage = (gameId, userId, name) => {
        connect().emit('join-to-game', {game_id: gameId})
        history.push(`/games/${gameId}`,{id: userId, name})
    }

    const createGame = () => {  
        let gameName = createGameInputRef.current.value;
        connect().emit('create-game',{name: gameName})
    }

    return (
        <Container>
            <div className={classes.main}>
                <h1>Games</h1> 
                <div>
                    <input ref={createGameInputRef} placeholder="choose your game name" />
                    <span onClick={createGame}>Create Game</span>
                </div>
            </div>
            <ul className={classes.list}>
                {state.map(game => (
                    <li key={game._id} onClick={goDoozePage.bind(this, game._id, location.state.id, game.name)} className={classes.item}> {game.name} </li>
                ))}
            </ul>
        </Container>
    )
}

export default Games
