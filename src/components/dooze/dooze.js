import { useEffect, useState } from 'react';
import classes from './dooze.module.css';
import { connect, socket } from '../../service/socket';
import { useLocation, useParams } from "react-router-dom";

const Dooze = props => {

    const [cells, setCells] = useState([])
    let { id } = useParams();
    const location = useLocation()

    useEffect(() => {
        
        const socket = connect("Socket initialed before !!")

        socket.on('fill', data => {
            data = data.data
            
            setCells(cells => {
                return [...cells, {role: data.role, row: data.row, col: data.col}]
            })

            if(data.win){
                alert("WWWWIIINNNNN")
            }
        })

        
    }, [])

    const onCellHandler = (row, col) => {
        const socket = connect("Socket initialed before !!")
        const gameId = id

        console.log(row, col, gameId)
        const data = {
            game_id: gameId,
            row,
            col
        }

        socket.emit('fill', data)
    }

    const cellStatus = (row, col) => {

        const exist = cells.filter(cell => (cell.row == row && cell.col == col))

        console.log(" ==>>> ", exist)

        if(exist.length){
            if(exist[0].role == 0)
                return (<div className="bg-success h-100 d-flex justify-content-center align-items-center">
                    
                </div>)
            return (<div className="bg-danger h-100">
                
            </div>)
        }
        else return <div></div> 

    }



    return (
        <div className={classes.main} >
            <h1>{location.state.name}</h1>
            <table className={classes.table} >
                <tr>
                    <td onClick={onCellHandler.bind(this, 1,1)}> {cellStatus(1,1)} </td>
                    <td onClick={onCellHandler.bind(this, 1,2)}> {cellStatus(1,2)} </td>
                    <td onClick={onCellHandler.bind(this, 1,3)}> {cellStatus(1,3)} </td>
                </tr>
                <tr>
                    <td onClick={onCellHandler.bind(this, 2,1)}> {cellStatus(2,1)} </td>
                    <td onClick={onCellHandler.bind(this, 2,2)}> {cellStatus(2,2)} </td>
                    <td onClick={onCellHandler.bind(this, 2,3)}> {cellStatus(2,3)} </td>
                </tr>
                <tr>
                    <td onClick={onCellHandler.bind(this, 3,1)}> {cellStatus(3,1)} </td>
                    <td onClick={onCellHandler.bind(this, 3,2)}> {cellStatus(3,2)} </td>
                    <td onClick={onCellHandler.bind(this, 3,3)}> {cellStatus(3,3)} </td>
                </tr>
                
            </table>
        </div>
    )
}

export default Dooze
