import Home from '../components/home/home';
import './App.css';
import { Redirect, Route, Switch } from 'react-router-dom'
import Games from '../components/games/games';
import Dooze from '../components/dooze/dooze'

function App() {
  return (
    <div className="App">
      <Switch>
        <Route path='/home'>
          <Home />
        </Route>

        <Route exact path='/games' >
          <Games />
        </Route>

        <Route path='/games/:id' >
          <Dooze />
        </Route>

        <Redirect to='/home' />
      
      </Switch> 
      
    </div>
  );
}

export default App;
